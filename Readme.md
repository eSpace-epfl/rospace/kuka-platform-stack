# Kuka base orbital sim

Package:
- relative_spherical_coordinat: Node to calcul and publish the relative position of the target
- satellite_pos_msgs: 			Message definition for the relative position of the target
- kuka_relative_pos : 			Folder for the stack files (kuka_relative_pos_stack)
- image_common : 				Message definition for images
- image_pipeline : 				Package for the cameras nodes
- kuka_controller: 				Node to control the iiwa
- camera_control_msgs: 			Message package from the pylon_camera package
- pylon_camera :				ROS-Driver for Basler Cameras 

# Cam node: pylon_camera (Basler ace)
http://wiki.ros.org/pylon_camera

Build Instructions

The pylon_camera-pkg requires the pylonSDK to be installed on your system. Please install the pylon debian package from:

https://www.baslerweb.com/en/support/downloads/software-downloads/pylon-5-0-9-linux-x86-64-bit-debian/

Build the pylon_camera package as you would build a standard ROS-package unsing: ``catkin build``

Github repo : 
- https://github.com/magazino/pylon_camera
- https://github.com/magazino/camera_control_msgs

Pylon_camera_node.lauch is used to start the node publishing the raw images on  "/pylon_camera_node/image_raw".
It also start the image processing node that rectified, crop (1600x1600) and published on: "camera_crop/image_rect_color"
The rectification is based on the calibration file located here: kuka_relative_pos_stack/pylon_camera/config/calib_cam.yaml


# Starting ROS and the robot iiwa
The satellite trajectory must have been teached before starting. Use the program "HandGuiding" for it.

1. Start the ROS master and communication server in a terminal: ``roslaunch relative_spherical_coordinate start_server.launch``
2. On hte robot, start the program:  « API_ROS_KUKA_V30032017 »
3. In a new terminal, start the calaculating node: ```roslaunch relative_spherical_coordinate start_rest_basler.launch```
4. On the robot SmartPad, vary the speed to start moving.

# Record or process data
To record the data, you can use a rosbag that save all or the selected topics. It is than easy to replay the senario after. Or use the python script: "rosrun relative_spherical_coordinate save_data.py -s 'save_path'" to save the relative positions in a text file and the images in a folder. 

If the packages: catkin_simple, space_msgs and space_msgs are in the workspace, you can directly process them py launching the python script: ``rosrun space_vision space_vision.py -s 'save_path'``

