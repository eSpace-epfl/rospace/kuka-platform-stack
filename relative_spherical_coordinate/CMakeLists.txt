cmake_minimum_required(VERSION 2.8.3)
project(relative_spherical_coordinate)

## Use C++11
#add_definitions(-std=c++11)
set(CMAKE_CXX_FLAGS "-std=c++11 ${CMAKE_CXX_FLAGS}")

## Find catkin macros and libraries
find_package(catkin REQUIRED COMPONENTS
    roscpp
	satellite_pos_msgs
	tf2_ros
	tf2
	tf
)


catkin_package(
 # INCLUDE_DIRS include
#  LIBRARIES
  CATKIN_DEPENDS
	satellite_pos_msgs
#  DEPENDS
)

include_directories(include ${catkin_INCLUDE_DIRS})

## Declare cpp executables
add_executable(${PROJECT_NAME}
  src/${PROJECT_NAME}_node.cpp
)

## Specify libraries to link executable targets against
target_link_libraries(${PROJECT_NAME}
  ${catkin_LIBRARIES}
)

