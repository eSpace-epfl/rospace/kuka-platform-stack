#include <ros/ros.h>
#include <tf/tf.h>
#include <tf/transform_listener.h>
#include <tf2_ros/transform_listener.h>
#include <tf2_ros/static_transform_broadcaster.h>
#include <satellite_pos_msgs/ScsStamped.h>
#include <math.h>
#include <iostream>
#include <fstream>
#include "std_msgs/String.h"

std::ofstream myfile;
satellite_pos_msgs::ScsStamped scsStamped;

int main(int argc, char** argv)
{
  ros::init(argc, argv, "RelativeSphericalCoordinate");
  ros::NodeHandle nodeHandle;

  tf2_ros::Buffer tfBuffer;
  //tf2_ros::TransformListener tfListener(tfBuffer);
  tf::TransformListener tfListener;
  ros::Publisher publisher = nodeHandle.advertise<satellite_pos_msgs::ScsStamped>("sat/state/SHC", 1);
  ros::Publisher chatter_pub = nodeHandle.advertise<std_msgs::String>("sat_SHC", 10);

  ros::Rate rate(10.0);

  ROS_INFO("Successfully launched node.");
  tf::StampedTransform transformStanped;

  while (nodeHandle.ok()) {
    try {
      tfListener.lookupTransform("cam","sat_center",ros::Time(0), transformStanped);

    } catch(tf2::LookupException &ex) {
      continue;
    } catch (tf2::TransformException &ex) {
      ROS_WARN("%s", ex.what());
      continue;
    }

    scsStamped.header.stamp = transformStanped.stamp_;
    scsStamped.header.frame_id = "0";

    double x = transformStanped.getOrigin().getX();
    double y = transformStanped.getOrigin().getY();
    double z = transformStanped.getOrigin().getZ();

    double rang =  sqrt(pow(x,2) + pow(y,2) + pow(z,2));
    double elevation = asin(y/rang)*(180/3.1415); // elevation
    double azimuth = atan(x/z)*(180/3.1415);

    scsStamped.scs.elevation = elevation;
    scsStamped.scs.azimuth = azimuth;
    scsStamped.scs.rang = rang;
    scsStamped.scs.orientation.x = transformStanped.getRotation().getX();
    scsStamped.scs.orientation.y = transformStanped.getRotation().getY();
    scsStamped.scs.orientation.z = transformStanped.getRotation().getZ();
    scsStamped.scs.orientation.w = transformStanped.getRotation().getW();

    publisher.publish(scsStamped);

    std_msgs::String msg;

    std::stringstream ss;
    ss <<     scsStamped.scs.rang <<"; "<< scsStamped.scs.azimuth <<"; " << scsStamped.scs.elevation << " ; ["
        << scsStamped.scs.orientation.x <<" "<< scsStamped.scs.orientation.y <<" "
         << scsStamped.scs.orientation.z <<" "<< scsStamped.scs.orientation.w << "] ; taken at: " << scsStamped.header.stamp;

    msg.data = ss.str();
    chatter_pub.publish(msg);

    rate.sleep();
    }

  myfile.close();
  return 0;
}
