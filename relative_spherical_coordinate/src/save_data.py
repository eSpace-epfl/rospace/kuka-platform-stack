#! /usr/bin/python
# Copyright (c) 2015, Rethink Robotics, Inc.

# Using this CvBridge Tutorial for converting
# ROS images to OpenCV2 images
# http://wiki.ros.org/cv_bridge/Tutorials/ConvertingBetweenROSImagesAndOpenCVImagesPython

# Using this OpenCV2 tutorial for saving Images:
# http://opencv-python-tutroals.readthedocs.org/en/latest/py_tutorials/py_gui/py_image_display/py_image_display.html

# rospy for the subscriber
import rospy
# ROS Image message
from sensor_msgs.msg import Image
from std_msgs.msg import String
# ROS Image message -> OpenCV2 image converter
from cv_bridge import CvBridge, CvBridgeError
# OpenCV2 for saving an image
import cv2
import argparse
import os, sys, time

# Instantiate CvBridge
bridge = CvBridge()
real_data_file = ""

class callbacks:

    def __init__(self, path):

        # Set up your subscriber and define its callback
        self.image_sub = rospy.Subscriber("/camera_crop/image_rect_color", Image, self.image_callback)
        self.data_sub = rospy.Subscriber("/sat_SHC", String, self.data_callback)

        self.real_data = None
        '''Path to real data file'''

        self.image_path = None
        '''Path to saving folder'''

        self.real_data_file = os.path.join(path, 'real_data.txt')
        self.img_path = os.path.join(path, 'original')

        if not os.path.exists(path):
            os.makedirs(path)
            os.makedirs(self.img_path)

            with open(self.real_data_file, 'w') as save_file:
                save_file.write("Format : timestamp; range; azim; elev; quat\n")

    def image_callback(self, picture):
    	#print("Received an image!")
    	try:
    	    # Convert your ROS Image message to OpenCV2
    	    cv2_img = bridge.imgmsg_to_cv2(picture, "bgr8")
    	except CvBridgeError, e:
    	    print(e)
        else: 
            # Fix the position so that it does not change anymore
            real_data_str = self.real_data

    	    current_time = time.time()
            with open(self.real_data_file, 'a') as save_file:
                save_file.write("{}; ".format(current_time) + real_data_str + '\n')

    	    # Save your OpenCV2 image as a jpeg 
    	    cv2.imwrite(os.path.join(self.img_path, 'img{}.png'.format(current_time)), cv2_img)
    	    print(os.path.join(self.img_path, 'img{}.png'.format(current_time)))

    def data_callback(self, data):
        self.real_data = str(data.data)

def main():

    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--path', default=None)
    args = vars(parser.parse_args())

    if args['path'] is not None:

        callbacks(args['path'])
        rospy.init_node('image_listener', anonymous=True)

        # Spin until ctrl + c
        rospy.spin()

if __name__ == '__main__':
    main()
