#!/usr/bin/env python

# KUKA API for ROS
version = 'V15032017'

# Marhc 2016 Saeid Mokaram  saeid.mokaram@gmail.com
# Sheffield Robotics    http://www.sheffieldrobotics.ac.uk/
# The university of sheffield   http://www.sheffield.ac.uk/

# This script generats a ROS node for comunicating with KUKA iiwa
# Dependencies: conf.txt, ROS server, Rospy, KUKA iiwa java SDK, KUKA iiwa robot.

# This application is intended for floor mounted robots.
#######################################################################################################################
from client_lib import *
import time, os
import rospy, tf, re

if __name__ == '__main__':
    # Making a connection object.
    my_client = kuka_iiwa_ros_client()
    rate = rospy.Rate(10.0)

    while (not my_client.isready):
        pass  # Wait until iiwa is connected zzz!

    # Initializing Tool 1
    my_client.send_command('setTool tool1')

    br = tf.TransformBroadcaster()

    #for i in range(1000):
    while not rospy.is_shutdown():
        os.system('clear')
        print cl_pink('\n==========================================')
        print cl_pink('<   <  < << SHEFFIELD ROBOTICS >> >  >   >')
        print cl_pink('==========================================')
        print cl_pink(' Read KUKA data demo')
        print cl_pink(' Version: ' + version)
        print cl_pink('==========================================\n')
    
        print '#####################################'
        print 'OperationMode\t=', my_client.OperationMode  # True when a collision has accured.
        print 'isCollision\t=', my_client.isCollision        # True when a collision has accured.
        print 'isCompliance\t=', my_client.isCompliance      # True when robot is in Compliance mode.
        print 'isMastered\t=', my_client.isMastered
        print 'isready\t=', my_client.isready                # True when robot is connected
        print 'isReadyToMove\t=', my_client.isReadyToMove    # True when robot can move, e.g. when the safety key is pressed...
    
        toolPos = my_client.ToolPosition
        camPos = my_client.CamPosition

        print 'ToolPosition\t=', toolPos      # Reading Tool cartesian position
        print 'CamPosition\t=', camPos      # Reading Tool cartesian position  
        print '#####################################'

        toolPosq = tf.transformations.quaternion_from_euler(toolPos[0][3], toolPos[0][4], toolPos[0][5], 'rzyx')
        camPosq = tf.transformations.quaternion_from_euler(camPos[0][3], camPos[0][4], camPos[0][5], 'rzyx')

        time = rospy.Time.now()
        br.sendTransform((toolPos[0][0], toolPos[0][1], toolPos[0][2]),
                         (toolPosq[0], toolPosq[1], toolPosq[2], toolPosq[3]),
                         time,
                         "SatPos",
                         "World")

        br.sendTransform((camPos[0][0], camPos[0][1], camPos[0][2]),
                         (camPosq[0], camPosq[1], camPosq[2], camPosq[3]),
                         time,
                         "CamPos",
                         "World")

        rate.sleep()
