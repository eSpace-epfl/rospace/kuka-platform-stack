#!/usr/bin/env python

from client_lib import *
import time, os
import rospy, tf, re

if __name__ == '__main__':
    # Making a connection object.
    my_client = kuka_iiwa_ros_client()
    rate = rospy.Rate(10.0)

    while (not my_client.isready):
        pass  # Wait until iiwa is connected zzz!

    # Initializing Tool 1
    my_client.send_command('setTool tool1')

    br = tf.TransformBroadcaster()

    while not rospy.is_shutdown():
        os.system('clear')
        print cl_pink('\n==========================================')
        print cl_pink('<   <  < <<    Received data     >> >  >   >')
        print cl_pink('==========================================')
    
        print '#####################################'
        print 'isMastered\t=', my_client.isMastered
        print 'isready\t=', my_client.isready    # True when robot is connected
        print 'isReadyToMove\t=', my_client.isReadyToMove    # True when robot can move
        print 'ToolPosition\t=', toolPos      # Reading Tool cartesian position
        print 'CamPosition\t=', camPos      # Reading Tool cartesian position  
        print '#####################################'

        toolPos = my_client.ToolPosition
        camPos = my_client.CamPosition
        toolPosq = tf.transformations.quaternion_from_euler(toolPos[0][3], toolPos[0][4], toolPos[0][5], 'rzyx')
        camPosq = tf.transformations.quaternion_from_euler(camPos[0][3], camPos[0][4], camPos[0][5], 'rzyx')

        time = rospy.Time.now()
        # Satellite corner frame
        br.sendTransform((toolPos[0][0]/1000, toolPos[0][1]/1000, toolPos[0][2]/1000),
                         (toolPosq[0], toolPosq[1], toolPosq[2], toolPosq[3]),
                         time,
                         "sat_corner",
                         "World")

        # Satellite center Offest 
        br.sendTransform((-0.05, 0.05, -0.05),
                         (0, 0, 0, 1),
                         time,
                         "sat_center",
                         "sat_corner")

        # Camera frame 
        br.sendTransform((camPos[0][0]/1000, camPos[0][1]/1000, camPos[0][2]/1000), 
                         (camPosq[0], camPosq[1], camPosq[2], camPosq[3]),
                         time,
                         "cam",
                         "World")
        rate.sleep()
