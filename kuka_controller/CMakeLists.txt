cmake_minimum_required(VERSION 2.8.3)
project(kuka_controller)

find_package(catkin_simple REQUIRED)
catkin_simple(ALL_DEPS_REQUIRED)

cs_install_scripts(src/server_V30032017.py)

cs_install()

cs_export()
